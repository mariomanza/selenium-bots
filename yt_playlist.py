import time
import requests

from bs4 import BeautifulSoup

from Bot import Bot
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

class Yt(Bot):
    def __init__(self, url):
        super().__init__(url)
        self.get_playlist()
        self.download_loop()
    
    def get_playlist(self):
        time.sleep(2)
        pl = self.get_el('ytd-playlist-panel-renderer.style-scope:nth-child(5)').get_attribute('innerHTML')
        soup = BeautifulSoup(pl, 'html.parser')
        self.playlist = []
        for i in soup.find_all("a", {"id": 'wc-endpoint'}):
            song = i['href'].split('&list')[0]
            self.playlist.append('https://www.youtube.com'+song)

    def download_loop(self, path='/home/mario/Music/'):
        for p in self.playlist:
            self.wd.get('https://ytmp3.cc/')
            time.sleep(1)
            self.get_el('#input').send_keys(p + Keys.RETURN)
            time.sleep(2)
            name = self.get_el('#title').text
            link = self.get_el('#buttons > a:nth-child(1)').get_attribute('href')
            r = requests.get(link, allow_redirects=True)
            open(path+name, 'wb').write(r.content)

if __name__ == "__main__":
    yt = Yt('https://www.youtube.com/watch?v=LOPCPUq9f_g&list=RDLOPCPUq9f_g&start_radio=1&ab_channel=SuLee')