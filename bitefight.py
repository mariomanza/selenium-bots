import time
import re
from random import randint

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from Bot import Bot

class Bite(Bot):

    def __init__(self, url, usr, pwd):
        super().__init__(url, load=False, headless=False)
        self.usr = usr
        self.pwd = pwd
        self.stats = {}

    def login(self):
        self.wd.get(self.url+'user/login')
        self.get_el('user', By.NAME).send_keys(self.usr)
        self.wd.find_element_by_name("pass").send_keys(self.pwd + Keys.RETURN)
        time.sleep(2)
        self.egg()

    def logout(self):
        try:
            time.sleep(.5)
            elem = self.get_el('#menuHead > li:nth-child(18) > a:nth-child(1)')
            self.scroll(elem)
            elem.click()
        except Exception as e:
            print(e)

    def egg(self):
        try:
            time.sleep(.5)
            self.wd.find_element_by_id('easterEgg').click()
            return True
        except Exception as e:
            return False

    def hunt(self):
        if self.stats['health'] < 3000:
            return 'health low'
        self.get_el('li.free-space:nth-child(6) > a:nth-child(1)').click()
        self.egg()
        self.get_el('div.btn-left:nth-child(3) > div:nth-child(1) > button:nth-child(1)').click()
        while True:
            self.egg()
            self.stats = self.get_stats()
            if self.stats['health'] < 3000:
                return 'health low'
            if self.stats['ap'] < randint(6,15):
                return 'ap low'
            try:
                self.get_el('/html/body/div[5]/div[2]/div[4]/div[2]/div/div/form/div/div/button', By.XPATH, 5).click()
            except:
                self.get_el('li.free-space:nth-child(6) > a:nth-child(1)').click()
                self.egg()
                self.get_el('/html/body/div[5]/div[2]/div[3]/div[2]/div[2]/div/div[2]/div/button', By.XPATH).click()

    def check_pot(self):
        try:
            self.get_el('.active > a:nth-child(1)').click()
            self.egg()
            self.get_el('#accordion > h3:nth-child(1) > a:nth-child(2)').click()
            html = self.get_el('#accordion > div:nth-child(2) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(2)').get_attribute('innerHTML')
            r = re.search(r':(.*?)C', html, re.MULTILINE).group(1)
            self.stats['potions'] = int(r)
            r = re.findall(r'n:(.*?)<', html, re.MULTILINE)
            t = list(map(int, r[1].split(':')))
            self.stats['pot_time'] = t[0]*60*60+t[1]*60+t[2]
            self.stats['pot_time_last'] = 0
        except:
            return 'no potions'

    def buy_pot(self):
        if self.stats['gold'] > 1000:
            self.get_el('#menuHead > li:nth-child(5) > a:nth-child(1)').click()
            self.egg()
            self.get_el('.table-wrap > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(2) > a:nth-child(1)').click()
            self.egg()
            self.get_el('#shopMenu > a:nth-child(2)').click()
            self.egg()
            self.get_el('#shopOverview > tbody:nth-child(1) > tr:nth-child(4) > td:nth-child(3) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)').click()
            self.egg()
            self.check_pot()

    def heal(self, potion=''):
        try:
            if (self.stats['potions'] > 0) and (time.perf_counter()-self.stats['pot_time_last'] > self.stats['pot_time']):
                self.get_el('#menuHead > li:nth-child(2) > a:nth-child(1)').click()
                self.egg()
                try:
                    self.get_el('div.btn-left:nth-child(12) > div:nth-child(1) > a:nth-child(1)').click()
                except:
                    self.get_el('#accordion > h3:nth-child(3) > a:nth-child(2)').click()
                    self.egg()
                    self.get_el('div.btn-left:nth-child(12) > div:nth-child(1) > a:nth-child(1)').click()
                self.stats['pot_time_last'] = int(time.perf_counter())
            else:
                self.get_el('#menuHead > li:nth-child(2) > a:nth-child(1)').click()
            return re.search(r'>(.*)<', self.get_el('healing_countdown', By.ID, 5).get_attribute('innerHTML'), re.MULTILINE).group(1)
        except Exception as e:
            print(e)
            return 'not healed'

    def get_stats(self):
        html = self.get_el('.gold').get_attribute('innerHTML')
        matches = re.finditer(r">.*?<", '>'+html+'<', re.MULTILINE | re.DOTALL)
        a = []
        for matchNum, match in enumerate(matches, start=1):
            a.append(re.sub(r"&nbsp;| |<|>|\n", '', match.group(), 0, re.MULTILINE | re.DOTALL))
        time.sleep(1)
        ht = a[4].split('/')
        ap = a[3].split('/')
        self.stats = {'gold':int(''.join(a[0].split('.'))),'gems':int(a[2]),'ap':int(''.join(ap[0].split('.'))),
        'ap_max':int(''.join(ap[1].split('.'))),'health':int(''.join(ht[0].split('.'))),'health_max':int(''.join(ht[1].split('.'))),'lvl':int(a[6])}
        self.check_pot()
        return self.stats

    def hunt_loop(self):
        hunt = self.hunt()
        if hunt == 'health low':
            if self.stats['lvl'] > 2:
                self.check_pot()
                if self.stats['potions'] < 1:
                    self.buy_pot()
            t = self.heal()
            if t != 'not healed':
                self.get_stats()
                if self.stats['health'] > 15000:
                    self.hunt_loop()
                t = list(map(int, t.split(':')))
                self.logout()
                time.sleep((t[0]*60*60+t[1]*60+t[2]+randint(2600,3600)))
                self.login()
                self.hunt_loop()
            else:
                self.logout()
                self.wd.quit()
        self.logout()
    

if __name__ == "__main__":
    #'https://s26-mx.bitefight.gameforge.com/'
    bot = Bite('https://s3-mx.bitefight.gameforge.com/','Alvarex','sexymexy45!')
    bot.login()
    bot.get_stats()
    print(bot.stats)
    bot.hunt_loop()
    print(bot.stats)
    bot.logout()
    #bot.wd.close()
