import random
import time

from bs4 import BeautifulSoup
from Bot import Bot

class Form(Bot):
    def __init__(self, url, opts=False):
        super().__init__(url)
        self.opts = opts
    
    def scan(self):
        html = self.get_el('.freebirdFormviewerViewFormContent').get_attribute('innerHTML')
        soup = BeautifulSoup(html, 'html.parser')
        questions = soup.findAll("div", {"class": "freebirdFormviewerViewNumberedItemContainer"})
        structure = []
        for q in questions:
            header = q.find("div", {"class": "freebirdFormviewerComponentsQuestionBaseHeader"})
            body = header.next_sibling()
            for i in body:
                if 'class' in i.attrs:
                    print(i['class'])
            structure.append({
                'type': "",
                'title': header.get_text(),
                'content': ""
                })
            break
        return structure


if __name__ == "__main__":
    f = Form('https://forms.gle/Xh1VKMqB3aBB4UU47')
    print(f.scan())
    f.wd.close()
