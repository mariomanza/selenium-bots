import time

from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class Bot:
    def __init__(self, url, load=True, profile=False, headless=True):
        options = Options()
        options.headless = headless
        if profile:
            options.profile = webdriver.FirefoxProfile('/home/mario/.mozilla/firefox/dg4xu0b8.default-esr78')
        self.wd = webdriver.Firefox(options=options)
        self.url = url
        if load:
            self.wd.get(url)

    def get_el(self, elem, meth=By.CSS_SELECTOR, wait=15):
        try:
            return WebDriverWait(self.wd, wait).until(EC.presence_of_element_located((meth, elem)))
        except Exception as e:
            print(e)
            self.wd.quit()

    def scroll(self, elem, top=False):
        time.sleep(1)
        js = "arguments[0].scrollIntoView(true);" if top else "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);var elementTop = arguments[0].getBoundingClientRect().top;window.scrollBy(0, elementTop-(viewPortHeight/2));"
        self.wd.execute_script(js, elem)

    def focus(self):
        time.sleep(4)
        self.wd.switch_to.window(self.wd.window_handles[0])